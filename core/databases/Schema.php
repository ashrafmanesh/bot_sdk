<?php

/**
 * Description of Schema
 *
 * @author ramin ashrafimanesh <ashrafimanesh@gmail.com>
 */
class Schema {
    public static function create($table_name,$function,$driver='mysqli'){
        if(!in_array($driver,['mysqli'])){
            die('invalid migration driver');
        }
        switch ($driver) {
            case 'mysqli':
                $obj=new MysqliSchemaTable($table_name);
                $function($obj);
                
                echo $obj->create().'<br/>';
                
                break;
            default:
                break;
        }
    }
    
    public static function table($table_name,$function,$driver='mysqli'){
        
    }
    
    public static function drop($table_name,$driver='mysqli'){
        
    }
}

abstract class SchemaStruct{
    protected $table,$fields,$primaryKey=null;
    public function __construct($table) {
        $this->table=$table;
    }
    abstract public function integer($field,$length=11);
    abstract public function tinyInt($field,$length=2);
    abstract public function bigInt($field,$length=20);
    abstract public function string($field,$length=255);
    abstract public function float($field,$decimal=6,$points=2);
    
    public function primeryKey($field){
        return $this->primaryKey=$field;
    }
    
    abstract public function create();
}

class SchemaField{
    public $field_name,$sql,$isPrimary=false;
    
    public function nullAble(){
        $this->sql.=' NULL';
        return $this;
    }
    
    public function notNull(){
        $this->sql.=' NOT NULL';
        return $this;
    }
    
    public function autoIncrement(){
        $this->sql.=' AUTO_INCREMENT';
        return $this;
    }
    
    public function primaryKey(){
        $this->isPrimary=true;
        return $this;
    }
}


class MysqliSchemaTable extends SchemaStruct{
    
    public function __construct($table) {
        parent::__construct($table);
    }
    
    public function create() {
        if(sizeof($this->fields)<=0){
            return '';
        }
        $sql='CREATE TABLE `'.$this->table.'` (';
        $indexes=',';
        foreach($this->fields as $field){
            $sql.=$field->sql.',';
            if($field->isPrimary){
                $indexes.="PRIMARY KEY (`".$field->field_name."`),";
            }
        }
        $sql=rtrim($sql,',');
        $indexes=rtrim($indexes,',');
        $sql.=$indexes.');';
        load_app_model('MigrationModel');
        $MigrationModel=new MigrationModel();
        $MigrationModel->db->query($sql);
        return $sql;
        
    }
    
    public function float($field,$decimal = 6, $points = 2) {
        $obj=new SchemaField();
        $obj->field_name=$field;
        $obj->sql=" `$field` FLOAT($decimal,$points)";
        $this->fields[]=&$obj;
        return $obj;
    }

    public function integer($field,$length = 11) {
        $obj=new SchemaField();
        $obj->field_name=$field;
        $obj->sql=" `$field` INT($length)";
        $this->fields[]=&$obj;
        return $obj;
    }

    public function string($field,$length = 255) {
        $obj=new SchemaField();
        $obj->field_name=$field;
        $obj->sql=" `$field` VARCHAR($length)";
        $this->fields[]=&$obj;
        return $obj;
    }

    public function tinyInt($field,$length = 2) {
        $obj=new SchemaField();
        $obj->field_name=$field;
        $obj->sql=" `$field` TINYINT($length)";
        $this->fields[]=&$obj;
        return $obj;
    }

    public function bigInt($field,$length = 20) {
        $obj=new SchemaField();
        $obj->field_name=$field;
        $obj->sql=" `$field` BIGINT($length)";
        $this->fields[]=&$obj;
        return $obj;
    }

}
