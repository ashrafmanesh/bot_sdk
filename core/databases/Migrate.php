<?php

/**
 * Description of Migrate
 *
 * @author ramin ashrafimanesh <ashrafimanesh@gmail.com>
 */
class Migrate {
    
    public static function run(){
        
        require_once 'Schema.php';
        
        load_app_model('MigrationModel');
        $MigrationModel=new MigrationModel();
        
        $migrations=dirToArray('database/migrations');
        if(sizeof($migrations)<=0){
            echo "\033[31m";
            print('Migration does not exist'."\n");
            echo "\033[30m"."\n";
            return;
        }
        
        $result=$MigrationModel->get_rows();
       
        $executed=array();
        foreach($result as $row){
            $executed[$row['path']]=$row;
        }
        $runing=array();
        foreach($migrations as $migration){
            if(isset($executed[$migration])){
                continue;
            }
            $runing[]=['path'=>$migration];
            $arr= explode('_', trim($migration,'.php'));
            $date=$arr[0];
            unset($arr[0]);
            $class=  implode('_', $arr);
            require_once 'database/migrations/'.$migration;
            $obj=new $class();
            $obj->up();
        }
        if(sizeof($runing)){
            $MigrationModel->insert($runing);
        }
        echo 'runned migrations: <br/>';
        dd($runing);
    }
}
